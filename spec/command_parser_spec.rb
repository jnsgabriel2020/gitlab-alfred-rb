require 'spec_helper'

require 'command_parser'

describe CommandParser do
  describe '#parse' do
    def parse(input)
      described_class.new(input).parse
    end

    describe 'special commands' do
      it 'parses `me`' do
        expect(parse('me'))
          .to eq "https://gitlab.com/liz.lemon"
      end
    end

    describe 'dashboard commands' do
      context 'with issues' do
        it 'parses `#assigned`' do
          expected = 'https://gitlab.com/dashboard/issues/?assignee_username=liz.lemon'

          expect(parse('#ass')).to eq(expected)
          expect(parse('#assigned')).to eq(expected)
        end

        it 'parses `#mine`' do
          expected = 'https://gitlab.com/dashboard/issues/?author_username=liz.lemon'

          expect(parse('#mine')).to eq(expected)
        end

        it 'parses `#todo`' do
          expected = 'https://gitlab.com/dashboard/issues/?assignee_username=liz.lemon&not[author_username]=liz.lemon'

          expect(parse('#todo')).to eq(expected)
        end
      end

      context 'with merge requests' do
        it 'parses `!assigned`' do
          expected = 'https://gitlab.com/dashboard/merge_requests/?assignee_username=liz.lemon'

          expect(parse('!ass')).to eq(expected)
          expect(parse('!assigned')).to eq(expected)
        end

        it 'parses `!mine`' do
          expected = 'https://gitlab.com/dashboard/merge_requests/?author_username=liz.lemon'

          expect(parse('!mine')).to eq(expected)
        end

        it 'parses `!todo`' do
          expected = 'https://gitlab.com/dashboard/merge_requests/?assignee_username=liz.lemon&not[author_username]=liz.lemon'

          expect(parse('!todo')).to eq(expected)
        end
      end

      context 'with todos' do
        it 'parses `todo` and `todos`' do
          expected = 'https://gitlab.com/dashboard/todos/'

          expect(parse('todo')).to eq(expected)
          expect(parse('todos')).to eq(expected)
        end
      end
    end

    describe 'project commands' do
      let(:project) { Configuration.projects.sample }
      let(:name)    { project.names.sample }
      let(:path)    { project.path }

      context 'without a type' do
        it 'parses `[project]`' do
          expect(parse(name))
            .to eq path
        end
      end

      context 'with issues' do
        it 'parses `[project] #`' do
          expect(parse("#{name} #"))
            .to eq "#{path}/issues"
        end

        it 'parses `[project] #new`' do
          expect(parse("#{name} #new"))
            .to eq "#{path}/issues/new"
        end

        it 'parses `[project] #1234`' do
          expect(parse("#{name} #1234"))
            .to eq "#{path}/issues/1234"
        end
      end

      context 'with merge requests' do
        it 'parses `[project] !`' do
          expect(parse("#{name} !"))
            .to eq "#{path}/merge_requests"
        end

        it 'parses `[project] !new`' do
          expect(parse("#{name} !new"))
            .to eq "#{path}/merge_requests/new"
        end

        it 'parses `[project] !1234`' do
          expect(parse("#{name} !1234"))
            .to eq "#{path}/merge_requests/1234"
        end
      end

      context 'with commits' do
        it 'parses `[project] @`' do
          expect(parse("#{name} @"))
            .to eq "#{path}/commits/master"
        end

        it 'parses `[project] @branch-name`' do
          expect(parse("#{name} @12-9-stable-ee"))
            .to eq "#{path}/commits/12-9-stable-ee"
        end

        it 'parses `[project] @a1b2c3d4`' do
          expect(parse("#{name} @a1b2c3d4"))
            .to eq "#{path}/commit/a1b2c3d4"
        end

        it 'strips input' do
          expect(parse("#{name} @ a1b2c3d4"))
            .to eq "#{path}/commit/a1b2c3d4"
        end
      end
    end
  end
end
