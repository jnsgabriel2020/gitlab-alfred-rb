require 'spec_helper'

require 'configuration_file'

describe ConfigurationFile do
  describe '.initialize' do
    it 'validates the path' do
      allow(described_class).to receive(:detect_configuration).and_return(nil)

      expect { described_class.new }
        .to raise_error(described_class::NotFoundError)
    end
  end

  describe '#parse' do
    it 'warns when YAML is invalid' do
      invalid = { foo: :foo, bar: :bar }
        .to_yaml
        .sub(':bar', '')

      allow(File).to receive(:read).and_return(invalid)

      expect { subject.parse }.not_to raise_error
    end

    it 'warns when multiple projects share a name'

    it 'warns when a name contains a space'

    it 'falls back to the example when no custom file exists'
  end
end
